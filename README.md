# Silverstripe Image Tweaks

This module includes several, opinionated improvements to work with images in Silverstripe

## Installation

```
composer require bhofstaetter/image-tweaks
```

### Installing the utilities on Ubuntu:

```sh
sudo apt install jpegoptim optipng pngquant gifsicle imagemagick
```

### Installing the utilities on Alpine Linux:

```sh
apk add jpegoptim optipng pngquant gifsicle imagemagick
```

## Contents

- ``PadMax`` image manipulation
- Scales down all uploaded images to 4K resolution
- Optimize all uploaded images
- Reduces CMS image thumbnail dimensions
- Reduces image quality to 90%
- + more features from included modules

## Configuration

See ``config.yml`` and ``injectsions.yml`` or refer to the included models readme file

## Included Modules

- [Scaled Uploads](https://github.com/axllent/silverstripe-scaled-uploads)
- [Image Optimiser](https://github.com/axllent/silverstripe-image-optimiser)
- [HTMLEditorSrcSet](https://github.com/bigfork/htmleditorsrcset)
- [FocusPoint](https://github.com/jonom/silverstripe-focuspoint)

## Todo

- check https://github.com/CITANZ/image-cropper
