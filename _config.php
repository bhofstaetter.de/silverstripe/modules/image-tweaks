<?php

use Axllent\ImageOptimiser\Flysystem\FlysystemAssetStore;
use Spatie\ImageOptimizer\Optimizers\Jpegoptim;

$assetsStoreConfig = FlysystemAssetStore::config();
$chains = $assetsStoreConfig->get('chains');
$chains[Jpegoptim::class][0] = '--max=90'; // also edit this value inside injections.yml file
$assetsStoreConfig->set('chains', $chains);
