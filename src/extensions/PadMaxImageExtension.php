<?php

namespace bhofstaetter\ImageTweaks;

use SilverStripe\Assets\Image_Backend;
use SilverStripe\Assets\Storage\AssetContainer;
use SilverStripe\Core\Extension;

class PadMaxImageExtension extends Extension {

    public function PadMax($width, $height, $backgroundColor = 'FFFFFF', $transparencyPercent = 100): AssetContainer {
        $owner = $this->owner;

        if (!$owner->exists()) {
            return $owner;
        }

        $width = (int) $width;
        $height = (int) $height;

        $currImgWidth = $owner->getWidth();
        $currImgHeight = $owner->getHeight();

        if ($currImgWidth === $width && $currImgHeight === $height) {
            return $owner;
        }

        if ($currImgWidth < $width) {
            if ($currImgWidth < $currImgHeight) {
                $owner = $owner->Pad($width, $currImgHeight, $backgroundColor, $transparencyPercent);
            } else {
                $owner = $owner->Pad($currImgWidth, $height, $backgroundColor, $transparencyPercent);
            }
        }

        $variant = $owner->variantName(__FUNCTION__, $width, $height, $backgroundColor, $transparencyPercent);

        return $owner->manipulateImage(
            $variant,
            function (Image_Backend $backend) use ($width, $height, $backgroundColor, $transparencyPercent) {
                if ($backend->getWidth() === $width && $backend->getHeight() === $height) {
                    return $this;
                }

                return $backend->paddedResize($width, $height, $backgroundColor, $transparencyPercent);
            }
        );
    }
}
